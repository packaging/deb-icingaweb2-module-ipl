#!/bin/sh
# see: dh_installdeb(1)

set -e

# summary of how this script can be called:
#        * <postinst> `configure' <most-recently-configured-version>
#        * <old-postinst> `abort-upgrade' <new version>
#        * <conflictor's-postinst> `abort-remove' `in-favour' <package>
#          <new-version>
#        * <postinst> `abort-remove'
#        * <deconfigured's-postinst> `abort-deconfigure' `in-favour'
#          <failed-install-package> <version> `removing'
#          <conflicting-package> <version>
# for details, see https://www.debian.org/doc/debian-policy/ or
# the debian-policy package

module_name=ipl

setperm() {
    user="$1"
    group="$2"
    mode="$3"
    file="$4"
    shift 4
    # only do something when no setting exists
    if ! dpkg-statoverride --list "$file" >/dev/null 2>&1; then
      chown "$user":"$group" "$file"
      chmod "$mode" "$file"
    fi
}

case "$1" in
    configure)
        if [ -z "$2" ]; then
            if [ ! -d /etc/icingaweb2/enabledModules ]; then
                echo "Creating directory /etc/icingaweb2/enabledModules"
                mkdir /etc/icingaweb2/enabledModules

                if ! dpkg-statoverride --list /etc/icingaweb2; then
                    setperm root icingaweb2 2770 /etc/icingaweb2/enabledModules
                fi
            fi

            echo "Enabling icingaweb2 module '${module_name}'"
            ln -svf /usr/share/icingaweb2/modules/"${module_name}" /etc/icingaweb2/enabledModules/"${module_name}"
        fi
    ;;

    abort-upgrade|abort-remove|abort-deconfigure)
    ;;

    *)
        echo "postinst called with unknown argument \`$1'" >&2
        exit 1
    ;;
esac

# dh_installdeb will replace this with shell code automatically
# generated by other debhelper scripts.

#DEBHELPER#

exit 0
